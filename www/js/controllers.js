angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('NoteCtrl', function($scope, Notes) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.notes = Notes.all();
  $scope.remove = function(note) {
    Notes.remove(note);
  };
})

.controller('NoteDetailCtrl', function($scope, $stateParams, Notes) {
  $scope.note = Notes.get($stateParams.noteId);
})
//Going to use this for adding a note
.controller('AddNoteCtrl', function($scope,$state, Notes) {


$scope.noteMaster = {
  id: new Date().getTime().toString(),
  name: '',
  lastText: '',
  face: 'img/ben.png'
};

$scope.note = {
   id: new Date().getTime().toString(),
  name: '',
  lastText: '',
  face: 'img/ben.png'
};


$scope.addNote = function (note) {

  
  Notes.add($scope.note);

 // alert('adding note:' + $scope.note.title + ":" + $scope.note.lastText);

  $scope.note = angular.copy($scope.noteMaster);

  $state.go('tab.notes');


}




});
